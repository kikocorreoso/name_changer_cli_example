name_changer_cli_example
========================

This is an example of a cli tool made using python.

Installation:
-------------

`git clone https://kikocorreoso@gitlab.com/kikocorreoso/name_changer_cli_example.git`

`cd name_changer_cli_example`

`pip install .`

Usage:
------

Large version:

`name_changer --startswith a --endswith .png --contains 2017 --replace 2017 --with 2018 --path .`

Short version:

`name_changer -s a -e .png -c 2017 -r 2017 -w 2018 -p .`

And all the files with its name: 

* starting with `a`
* ending with `.png`
* containing `2017`

in the current folder (`.`) will be named again changing `2017` with `2018`.

For instance, using the previous command, if the current directory contains:

```
abc_2017_001.png
abc_2017_002.png
abc_2017_003.png
abc_2017_004.png
abc_2017_005.png
abc_2017_006.png
abc_2017_007.png
def_2017_001.png
def_2017_002.png
def_2017_003.png
```

The first seven files will be renamed 
and the final result would be something like:

```
abc_2018_001.png
abc_2018_002.png
abc_2018_003.png
abc_2018_004.png
abc_2018_005.png
abc_2018_006.png
abc_2018_007.png
def_2017_001.png
def_2017_002.png
def_2017_003.png
```